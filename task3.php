<?php

class MyCalculator {

    public $value1, $value2;

    public function __construct( $value1, $value2 ) {

        $this->value1 = $value1;
        $this->value2 = $value2;
    }

    public function add() {

        return $this->value1 + $this->value2;
    }

    public function subtract() {

        return $this->value1 - $this->value2;
    }

    public function multiply() {

        return $this->value1 * $this->value2;
    }

    public function divide() {

        return $this->value1 / $this->value2;
    }
}

$mycalc = new MyCalculator(12, 6);

echo "Addition = " . $mycalc-> add() . "<br/>";
echo "Multiply = " . $mycalc-> multiply() . "<br/>";
echo "Subtraction = " . $mycalc-> subtract() . "<br/>";
echo "Division = " . $mycalc-> divide() . "<br/>"."\n";
?>